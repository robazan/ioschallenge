//
//  APIService.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//


import Alamofire
import SwiftyJSON

class APIService: NSObject {
static let sharedInstance = APIService()
    
    func requesBrand(success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void)
    {
        let strURL = "http://demo0065335.mockable.io/partners"
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            if responseObject.result.isSuccess {
                let jsonResponse = JSON(responseObject.result.value!)
                success(jsonResponse)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }

    
    
}
