//
//  SelectProduct.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/2/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class SelectProduct: NSObject {

    let brand: String
    let name: String
    let brandId: String
    let price: Float
    
    init(brand: String, name: String,price: Float,brandId: String) {
        self.brand = brand
        self.name = name
        self.price = price
        self.brandId = brandId
    }
    
}
