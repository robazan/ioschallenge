//
//  Product.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class Product: NSObject {

    let sku: String
    let name: String
    let enable: Bool
    let price: Float
    
    init(sku: String, name: String,price: Float,enable: Bool) {
        self.sku = sku
        self.name = name
        self.price = price
        self.enable = enable
    }
    
}
