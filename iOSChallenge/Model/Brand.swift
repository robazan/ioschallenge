//
//  Brand.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class Brand: NSObject {
    
    
    let name: String
    let logo: String
    let cover: String
    let keys: String
    let schedule: String
    let id: String
    let products : Array<Product>
    
    init(name: String, logo: String,cover: String,keys: String,schedule: String,id: String, products: Array<Product>) {
        self.name = name
        self.logo = logo
        self.cover = cover
        self.keys = keys
        self.schedule = schedule
        self.id = id
        self.products = products
    }
    
}
