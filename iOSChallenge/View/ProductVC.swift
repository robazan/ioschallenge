//
//  ProductVC.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import CoreData
class ProductVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var productsTableView: UITableView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var linkYouTube: UIButton!
    
    var brand: Brand!
    var context : NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productsTableView.delegate = self
        self.productsTableView.dataSource = self
        self.registerProductCell()
        self.renderUI()
        // Do any additional setup after loading the view.
    }
    
    func renderUI() -> Void {
        nameLbl.text = brand.name
        logoImg.sd_setImage(with: URL(string: brand.logo), completed: nil)
    }
    
    @IBAction func toYouTube(_ sender: Any) {
        
        let videoId = "bFCt4qBgl1A"
        var url = URL(string:"youtube://\(videoId)")!
        if !UIApplication.shared.canOpenURL(url)  {
            url = URL(string:"http://www.youtube.com/watch?v=\(videoId)")!
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: - Table view Cell
    
    func registerProductCell() -> Void {
        let brandCell = UINib(nibName: "ProductCell", bundle: nil)
        self.productsTableView.register(brandCell, forCellReuseIdentifier: "ProductCell")
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.brand.products.count
        //return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = self.brand.products[indexPath.row]
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as? ProductCell
        
       
        cell?.nameLbl.text = product.name
        cell?.priceLbl.text = "$ \(product.price)"
        
        return cell!
        

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        
        let product = self.brand.products[indexPath.row]
        
        self.context = appDelegate.persistentContainer.viewContext
        self.context.reset()
        let entity = NSEntityDescription.entity(forEntityName: "SelectedProduct", in: self.context)
        let selectedProd = NSManagedObject(entity: entity!, insertInto: self.context)
        
        selectedProd.setValue(product.name, forKey: "name")
        selectedProd.setValue(product.price, forKey: "price")
        selectedProd.setValue(brand.name, forKey: "brand")
        selectedProd.setValue(brand.id, forKey: "brandId")
        
        do {
            try context.save()
            let alert = UIAlertController(title: "Agregado", message: "El producto ha sido agregado a su carrito.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
        } catch {
            let alert = UIAlertController(title: "Error", message: "No se puede seleccionar este producto.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
