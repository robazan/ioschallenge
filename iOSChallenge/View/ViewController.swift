//
//  ViewController.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 1/29/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import CoreData

class ViewController: UIViewController,CLLocationManagerDelegate {


    @IBOutlet var gmapView: GMSMapView!
    let locationManager = CLLocationManager()
    var markerUser = GMSMarker()
    var context : NSManagedObjectContext!
    
    @IBOutlet weak var addessLbl: UILabel!
    
    override func viewDidLoad()
    {
        super .viewDidLoad()

        gmapView.isMyLocationEnabled = true
        gmapView.settings.myLocationButton = true
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    @IBAction func checkLocation(_ sender: Any) {
        locationManager.startUpdatingLocation()
    }
    

    @IBAction func saveLocation(_ sender: Any) {
        do {
            try context.save()
            performSegue(withIdentifier: "toMain", sender: self)
        } catch {
            print("Failed saving")
        }
        
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
//        //request.predicate = NSPredicate(format: "age = %@", "12")
//        request.returnsObjectsAsFaults = false
//        do {
//            let result = try context.fetch(request)
//            for data in result as! [NSManagedObject] {
//                print(data.value(forKey: "address") as! String)
//            }
//
//        } catch {
//
//            print("Failed")
//        }
        
    }
    
    private func getAddress(coordinate: CLLocationCoordinate2D) {

        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            self.addessLbl.text = lines.joined(separator: "\n")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            self.context = appDelegate.persistentContainer.viewContext
            self.context.reset()
            let entity = NSEntityDescription.entity(forEntityName: "Location", in: self.context)
            let userLocation = NSManagedObject(entity: entity!, insertInto: self.context)
            
            userLocation.setValue( self.addessLbl.text, forKey: "address")
            userLocation.setValue(coordinate.latitude, forKey: "latitude")
            userLocation.setValue(coordinate.longitude, forKey: "longitude")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        
        gmapView.camera = GMSCameraPosition(target: locValue, zoom: 15, bearing: 0, viewingAngle: 0)
        
        

        markerUser.position = CLLocationCoordinate2D(latitude: locValue.latitude, longitude:locValue.longitude)
        markerUser.title = "Ubicación del usuario"
        markerUser.snippet = "Esta es la ubicación actual del usuario "
        markerUser.map = gmapView
        
        getAddress(coordinate:locValue)
        
        
        locationManager.stopUpdatingLocation()
    }
}

