//
//  SelectedProductsVC.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import CoreData

class SelectedProductsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
  

    

    @IBOutlet weak var selectedProductsTableView: UITableView!
        var context : NSManagedObjectContext!
    var selectedProducts = [SelectProduct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedProductsTableView.delegate = self
        selectedProductsTableView.dataSource = self
        self.registerProductCell()
        self.getAllSelectedProducts()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view Cell
    
    func registerProductCell() -> Void {
        let brandCell = UINib(nibName: "ProductCell", bundle: nil)
        self.selectedProductsTableView.register(brandCell, forCellReuseIdentifier: "ProductCell")
    }
    
    // MARK: - Request CoreData
    func getAllSelectedProducts()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        self.context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SelectedProduct")
                //request.predicate = NSPredicate(format: "age = %@", "12")
                request.returnsObjectsAsFaults = false
                do {
                    let result = try context.fetch(request)
                    for data in result as! [NSManagedObject] {
                        let name = data.value(forKey: "name") as! String
                        let brand = data.value(forKey: "brand") as! String
                        let brandId = data.value(forKey: "brandId") as! String
                        let price = data.value(forKey: "price") as! Float
                        let selProd = SelectProduct(brand: brand, name: name,price: price,brandId: brandId)
                        
                        selectedProducts.append(selProd)
                    }
                    selectedProductsTableView.reloadData()
        
                } catch {
        
                    print("Failed")
                }
    }
    
    
    // MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedProducts.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = self.selectedProducts[indexPath.row]
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as? ProductCell
        
        
        cell?.nameLbl.text = "\(product.name) de \(product.brand) "
        cell?.priceLbl.text = "$ \(product.price)"
        
        return cell!
    
    }

}
