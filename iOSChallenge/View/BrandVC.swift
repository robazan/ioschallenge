//
//  BrandVC.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import SDWebImage

class BrandVC: UITableViewController, UIGestureRecognizerDelegate {

    @IBOutlet var brandTableView: UITableView!
    
    var brands = [Brand]()
    var indexSelected : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        registerBrandCell()
        getAllBrands()
        initLongPress()
    }

    // MARK: - LongPress Modal
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {

        if longPressGestureRecognizer.state == .ended {
            let touchPoint = longPressGestureRecognizer.location(in: self.brandTableView)
            if let indexPath = brandTableView.indexPathForRow(at: touchPoint) {
                indexSelected = indexPath.row
                self.performSegue(withIdentifier: "longPressSegue", sender: self)
            }
        }
    }
    
    func initLongPress() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.brandTableView.addGestureRecognizer(longPressGesture)
    }
    

     // MARK: - Request
    
    func getAllBrands() -> Void
    {
        APIService.sharedInstance.requesBrand(success: { (JSON) in
            
            for brand in JSON["result"].arrayValue {
                let id = brand["id"].stringValue
                let name = brand["name"].stringValue
                let logo = brand["logo"].stringValue
                let cover = brand["cover"].stringValue
                let keys = brand["keys"].stringValue
                let schedule = brand["schedule"].stringValue
                
                var productArray: [Product] = []
                productArray.removeAll()
                
                for product in brand["products"].arrayValue
                {
                    let sku = product["sku"].stringValue
                    let name = product["name"].stringValue
                    let price = product["price"].floatValue
                    let enable = product["enable"].boolValue
                    let prod = Product(sku: sku,name: name, price: price,enable: enable )
                    
                    productArray.append(prod)
                }
                
                let b = Brand(name: name, logo: logo, cover: cover, keys: keys, schedule: schedule, id: id, products: productArray)
                
                self.brands.append(b)
            }
            self.brandTableView.reloadData()
        }) { (Error) in
            
        }
    }
    
    // MARK: - Table view Cell
    
    func registerBrandCell() -> Void {
        let brandCell = UINib(nibName: "BrandCell", bundle: nil)
        self.brandTableView.register(brandCell, forCellReuseIdentifier: "BrandCell")
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.brands.count
        //return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let brand = brands[indexPath.row]
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "BrandCell") as? BrandCell
       
        cell?.brandCover!.sd_setImage(with: URL(string:brand.cover), completed: nil)
        cell?.brandLogo!.sd_setImage(with:  URL(string: brand.logo), completed: nil)
        cell?.brandName.text = brand.name
        cell?.brandSchedule.text = brand.schedule
        
        return cell!

       
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                self.performSegue(withIdentifier: "showDetail", sender: self)
    }
    

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetail")
        {
            // upcoming is set to NewViewController (.swift)
            let nextVC: ProductVC = segue.destination
                as! ProductVC
            let indexPath = self.brandTableView.indexPathForSelectedRow
            let brandSelected = brands[(indexPath?.row)!]
            nextVC.brand = brandSelected
        }
        if segue.identifier == "longPressSegue" {
            let nextVC: ProductModalDetailVC = segue.destination
                as! ProductModalDetailVC
            let brandSelected = brands[indexSelected]
            nextVC.brand = brandSelected
        }
    }
    


}
