//
//  BrandCell.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/1/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class BrandCell: UITableViewCell {

    @IBOutlet weak var brandLogo: UIImageView!
    @IBOutlet weak var brandCover: UIImageView!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var brandSchedule: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
