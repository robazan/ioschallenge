//
//  ProductModalDetailVC.swift
//  iOSChallenge
//
//  Created by Rodrigo Bazan on 2/2/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class ProductModalDetailVC: UIViewController {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    var brand: Brand!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.renderUI()
        // Do any additional setup after loading the view.
    }
    
    func renderUI() -> Void {
        nameLbl.text = brand.name
        logoImg.sd_setImage(with: URL(string: brand.logo), completed: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
