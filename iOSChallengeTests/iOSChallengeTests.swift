//
//  iOSChallengeTests.swift
//  iOSChallengeTests
//
//  Created by Rodrigo Bazan on 1/29/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import XCTest
@testable import iOSChallenge

class iOSChallengeTests: XCTestCase {

    var brand:Brand!
    
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let prod = Product(sku: "00001", name: "Producto", price: 1.0, enable: true)
        var products: [Product] = []
        products.append(prod)
        brand = Brand(name: "Marca", logo: "imagenprueba.png", cover: "coverprueba.png", keys: "tag - tag", schedule: "11", id: "0001", products: products)
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitProduct() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.

    }

}
